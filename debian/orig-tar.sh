#!/bin/sh

set -eu

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
DIR=${PACKAGE}-${VERSION}
TAR=../${PACKAGE}_${VERSION}.orig.tar.xz
TAG=$(echo "ASM_$VERSION" | sed -re's,\.,_,g')

rm $3
svn export svn://svn.forge.objectweb.org/svnroot/asm/tags/${TAG}/ $DIR
XZ_OPT=--best tar -c -J -f $TAR --exclude 'config/*.jar' $DIR
rm -rf $DIR
